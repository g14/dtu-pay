#Author: Rishabh Narang
#!/bin/bash
set -e

# Build and install the libraries
# abstracting away from using the
# RabbitMq message queue
#pushd /libraries/messaging-utilities
#./build.sh
#popd 

pushd /var/lib/jenkins/workspace/token-management
./build.sh
popd

pushd /var/lib/jenkins/workspace/account
./build.sh
popd

pushd /var/lib/jenkins/workspace/transactions
./build.sh
popd

pushd /var/lib/jenkins/workspace/PayProjectNew
./build.sh
popd

# Update the set of services and
# build and execute the system tests
pushd end-to-end-test
./deploy.sh 
#sleep 20s
./test.sh
popd

# Cleanup the build images
docker image prune -f

