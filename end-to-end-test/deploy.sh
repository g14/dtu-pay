#!/bin/bash
set -e
docker image prune -f
docker-compose up -d broker
sleep 15
docker-compose up -d token-management
docker-compose up -d account
docker-compose up -d transactions
docker-compose up -d payment-management

