Feature: PaymentSteps

  Scenario: Correct payment
    Given two customers with a bank account
    When the first customer transfers money to the second customer
    Then the money transfer succeeds

  Scenario: Not enough balance
  	Given two customers with a bank account
  	When the customer tries to transfer too much money to the second customer
  	Then the money transfer fails