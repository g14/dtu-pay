#Author: Rishabh Narang
Feature: Token Management
  Scenario: Token creation successful
	  When a rest request of 2 tokens is made by customer with id "5"
	  Then i return 2 tokens
	Scenario: Token creation unsuccessful
		Given a customer with id "10" who has 3 existing unused tokens
	  When a rest request of 2 tokens is made by the customer
	  Then it fails
	Scenario: Token creation unsuccessful
		Given a customer with id "11" who has 1 existing unused tokens
	  When a rest request of 6 tokens is made by the customer
	  Then it fails
	Scenario: Token creation unsuccessful
		Given a customer with id "13" who has 1 existing unused tokens
	  When a rest request of 0 tokens is made by the customer
	  Then it fails
	  