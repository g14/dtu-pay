Feature: Test rest API


  Scenario: GetAllTransactions
    Given a transaction database
    When fetching all transactions
    Then all transactions are fetched

  Scenario: get all merchant transactions
    Given a transaction database
    And an ID 1
    When fetching all merchant transactions
    Then all merchant transactions are fetched

  Scenario: get all merchant transactions within date
    Given a transaction database
    And an ID 2
    And a from date "2021-01-01 00:00:00"
    And a to date "2021-01-31 00:00:00"
    When fetching all merchant transactions within dates
    Then all merchant transactions within dates are fetched

  Scenario: get all customer transactions
    Given a transaction database
    And an ID 2
    When fetching all customer transactions
    Then all customer transactions are fetched

  Scenario: get all customer transactions within date
    Given a transaction database
    And an ID 1
    And a from date "2021-01-01 00:00:00"
    And a to date "2021-01-31 00:00:00"
    When fetching all customer transactions within dates
    Then all customer transactions within dates are fetched
