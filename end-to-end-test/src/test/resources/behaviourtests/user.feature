Feature: Test rest API

  Scenario: Register customer
    Given the cpr number "123456-9191"
    And the bank id "1234"
    And the first name "1234"
    And the last name "1234"
    When registering the customer
    Then the customer is registered

  Scenario: Register customer fails
    Given the cpr number "123456-7890"
    And the bank id "1234"
    And the first name "1234"
    And the last name "1234"
    When registering the customer
    Then the customer is not registered

  Scenario: Register merchant
    And the cvr number "123456-9191"
    And the bank id "1234"
    And the first name "1234"
    And the last name "1234"
    When registering the merchant
    Then the merchant is registered

  Scenario: Register merchant fails
    And the cvr number "123456-7890"
    And the bank id "1234"
    And the first name "1234"
    And the last name "1234"
    When registering the merchant
    Then the merchant is not registered

  Scenario: Update customer
    Given the id 1
    And the cpr number "123456-7890"
    And the bank id "4cff243b-b466-4a74-a34b-9dffa7890305"
    And the first name "hello"
    And the last name "world"
    When updating the customer
    Then the customer is updated

  Scenario: Update customer fail
    Given the id 5
    And the cpr number "123456-7890"
    And the bank id "1234"
    And the first name "1234"
    And the last name "1234"
    When updating the customer
    Then the customer is not updated

  Scenario: Update merchant
    Given the id 1
    And the cvr number "123456-7890"
    And the bank id "616b3e2e-5fca-425c-af55-50af267d1f27"
    And the first name "hello"
    And the last name "world"
    When updating the merchant
    Then the merchant is updated

  Scenario: Update merchant fail
    Given the id 5
    And the cvr number "123456-7890"
    And the bank id "1234"
    And the first name "1234"
    And the last name "1234"
    When updating the merchant
    Then the merchant is not updated