package behaviourtests;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

public class Service {

	public boolean runServiceFromPayment(String token, int amount) {
		Client client = ClientBuilder.newClient();
	     WebTarget r =
	      client.target("http://g-14.compute.dtu.dk:8081");
	     var response = r.path("payment").queryParam("token",token).queryParam("merchantid", "2").queryParam("amount", amount).queryParam("description", "end to end test").request().get(Boolean.class);
	     return response;
	}
}
