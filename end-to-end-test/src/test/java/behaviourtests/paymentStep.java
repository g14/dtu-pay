package behaviourtests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import behaviourtests.clientstubs.TokenCreationService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class paymentStep {
    Service service;
    String token;
    boolean response;

    @Given ("two customers with a bank account")
    public void ACustomerWithABankAccount(){
    	//implied by project description
    }
    
    @When("the first customer transfers money to the second customer")
    public void theFirstCustomerTransfersMoneyToTheSecondCustomer (){
    	TokenCreationService tokenService = new TokenCreationService();
    	String token=tokenService.createTokensFromTokenManagement("1", 1).get(0);
        service = new Service();
        response = service.runServiceFromPayment(token, 0);
    }
    @Then("the money transfer succeeds")
    public void TheMoneyTransferSucceeds(){
        assertTrue(response);
    }

    @When("the customer tries to transfer too much money to the second customer")
    public void theCustomerTriesToTransferTooMuchMoneyToTheSecondCustomer() {
    	TokenCreationService tokenService = new TokenCreationService();
    	String token=tokenService.createTokensFromTokenManagement("1", 1).get(0);
        service = new Service();
        response = service.runServiceFromPayment(token, Integer.MAX_VALUE);
    }
    
    @Then("the money transfer fails")
    public void theMoneyTransferFails() {
    	assertFalse(response);
    }
}
