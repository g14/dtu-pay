package behaviourtests.clientstubs;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;

//@Author Rishabh Narang
public class TokenCreationService {

	public List<String> createTokensFromTokenManagement(String customerId, int amount) {
		Client client = ClientBuilder.newClient();
		// TODO change this to localhost when pushing to git
		WebTarget r = client.target("http://localhost:8080/");
		List<String> tokens = r.path("token").path(customerId).path(String.valueOf(amount)).request()
				.get(new GenericType<List<String>>() {
				});
		return tokens;
	}
}
