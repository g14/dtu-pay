package behaviourtests;

import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionSteps {
    int ID;
    String fromDate;
    String toDate;

    Client client;
    WebTarget r;
    Response response;

    @Before
    public void beforeScenario() {
        client = ClientBuilder.newClient();
        r = client.target("http://g-14.compute.dtu.dk:9090/transactions/");
    }


    @Given("a transaction database")
    public void aTransactionDatabase() {
        //Ey! I ain't doing nuthin'.
    }


    @When("fetching all transactions")
    public void fetchingAllTransactions() {
        response = r.path("getTransactions").request().get();

    }

    @Then("all transactions are fetched")
    public void allTransactionsAreFetched() {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @And("an ID {int}")
    public void anID(int id) {
        this.ID = id;
    }

    @When("fetching all merchant transactions")
    public void fetchingAllMerchantTransactions() {
        response = r.path("getMerchantTransactions")
                .queryParam("id",this.ID)
                .request().get();
    }

    @Then("all merchant transactions are fetched")
    public void allMerchantTransactionsAreFetched() {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    }

    @And("a from date {string}")
    public void aFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    @And("a to date {string}")
    public void aToDate(String toDate) {
        this.toDate = toDate;
    }

    @When("fetching all merchant transactions within dates")
    public void fetchingAllMerchantTransactionsWithinDates() {
        response = r.path("getMerchantTransactionsWithinDates")
                .queryParam("id",this.ID)
                .queryParam("fromDate",this.fromDate)
                .queryParam("toDate",this.toDate)
                .request().get();
    }

    @Then("all merchant transactions within dates are fetched")
    public void allMerchantTransactionsWithinDatesAreFetched() {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    }

    @When("fetching all customer transactions")
    public void fetchingAllCustomerTransactions() {
        response = r.path("getCustomerTransactions")
                .queryParam("id",this.ID)
                .request().get();
    }

    @Then("all customer transactions are fetched")
    public void allCustomerTransactionsAreFetched() {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    }

    @When("fetching all customer transactions within dates")
    public void fetchingAllCustomerTransactionsWithinDates() {
        response = r.path("getCustomerTransactionsWithinDates")
                .queryParam("id",this.ID)
                .queryParam("fromDate",this.fromDate)
                .queryParam("toDate",this.toDate)
                .request().get();
    }

    @Then("all customer transactions within dates are fetched")
    public void allCustomerTransactionsWithinDatesAreFetched() {
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

    }
}
