package behaviourtests.messaging.service;

import behaviourtests.messaging.Event;
import behaviourtests.messaging.rabbitmq.RabbitMqListener;
import behaviourtests.messaging.rabbitmq.RabbitMqSender;

public class AccountMessageTest {

    public static void main(String[] args) {
        int id = 1;
        Service service = new Service();
        RabbitMqSender rabbitMqSender = new RabbitMqSender();
        service.setSender(rabbitMqSender);
        Event event = new Event("CustomerIdQueue", new Object[]{id});


        try {

            service.sendEvent(event);
        } catch (Exception e) {
            e.printStackTrace();
        }



    }
}
