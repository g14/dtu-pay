package behaviourtests.messaging.service;


import behaviourtests.messaging.Event;
import behaviourtests.messaging.EventReceiver;
import behaviourtests.messaging.EventSender;
import behaviourtests.messaging.rabbitmq.RabbitMqSender;

public class Service implements EventSender, EventReceiver {
    EventSender sender;
    @Override
    public void receiveEvent(Event event) throws Exception {

    }

    public void setSender(EventSender sender) {
        this.sender = sender;
    }


    @Override
    public void sendEvent(Event event) throws Exception {
        sender.sendEvent(event);
    }
}
