package behaviourtests.messaging.rabbitmq;


import behaviourtests.messaging.Event;
import behaviourtests.messaging.EventSender;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import io.cucumber.messages.internal.com.google.gson.Gson;


public class RabbitMqSender implements EventSender {

	private static final String EXCHANGE_NAME = "eventsExchange";
	private static final String QUEUE_TYPE = "topic";
	private static final String TOPIC = "CustomerIdQueue";

	@Override
	public void sendEvent(Event event) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();


		factory.setUri("amqp://guest:guest@g-14.compute.dtu.dk:5672");

		


		try (Connection connection = factory.newConnection(); Channel channel = connection.createChannel()) {
			channel.exchangeDeclare(EXCHANGE_NAME, QUEUE_TYPE);
			String message = new Gson().toJson(event);
			System.out.println("[x] sending "+message);
			channel.basicPublish(EXCHANGE_NAME, TOPIC, null, message.getBytes("UTF-8"));
		}
	}

}