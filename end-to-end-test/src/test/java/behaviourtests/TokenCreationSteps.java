package behaviourtests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import javax.ws.rs.ForbiddenException;

import behaviourtests.clientstubs.TokenCreationService;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

//@Author Rishabh Narang
public class TokenCreationSteps {

	private TokenCreationService service = new TokenCreationService();
	private List<String> tokens;
	private String customerId;
	private int initialTokensWithCustomer;
	private Exception resultException;

	@When("a rest request of {int} tokens is made by customer with id {string}")
	public void aRestRequestOfTokensIsMadeByCustomerWithId(Integer requestedTokens, String customerId) {
		tokens = service.createTokensFromTokenManagement(customerId, requestedTokens);
	}

	@Then("i return {int} tokens")
	public void iReturnTokens(Integer returnedNumberOfTokens) {
		// Write code here that turns the phrase above into concrete actions
		assertEquals(returnedNumberOfTokens, tokens.size());

	}

	@Given("a customer with id {string} who has {int} existing unused tokens")
	public void aCustomerWithIdWhoHasExistingUnusedTokens(String customerId, Integer amountOfUnusedTokens) {
		this.customerId = customerId;
		this.initialTokensWithCustomer = amountOfUnusedTokens;
		tokens = service.createTokensFromTokenManagement(customerId, amountOfUnusedTokens);
	}

	@When("a rest request of {int} tokens is made by the customer")
	public void aRestRequestOfTokensIsMadeByTheCustomer(Integer amountOfTokensRequested) {
		try {
			service.createTokensFromTokenManagement(customerId, amountOfTokensRequested);
		} catch (Exception e) {
			resultException = e;
		}
	}

	@Then("it fails")
	public void itFails() {
		assertTrue(ForbiddenException.class.isInstance(resultException));
	}

}
