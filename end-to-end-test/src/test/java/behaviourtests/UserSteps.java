package behaviourtests;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class UserSteps {
    Integer id;
    String cpr;
    String cvr;
    String bank_id;
    String firstName;
    String lastName;

    Client client;
    WebTarget r;
    Response response;

    @Before
    public void beforeScenario() {
        client = ClientBuilder.newClient();
        r = client.target("http://g-14.compute.dtu.dk:8585/user/");
    }

    @After
    public void beforeEachScenario() {
        r.path("deleteCustomer")
                .queryParam("id", (Integer) 3)
                .request()
                .method("delete");
        r.path("deleteMerchant")
                .queryParam("id", (Integer) 3)
                .request()
                .method("delete");
    }

//    @Given("the id {string}")
//    public void theId(String id) {
//        this.id = id;
//    }

    @Given("the cpr number {string}")
    public void theCprNumber(String cpr) {
        this.cpr = cpr;
    }

    @Given("the id {int}")
    public void theId(int arg0) {
        this.id = arg0;
    }

    @And("the cvr number {string}")
    public void theCvrNumber(String cvr) {
        this.cvr = cvr;
    }

    @And("the bank id {string}")
    public void theBankId(String bank_id) {
        this.bank_id = bank_id;
    }

    @And("the first name {string}")
    public void theFirstName(String firstName) {
        this.firstName = firstName;
    }

    @And("the last name {string}")
    public void theLastName(String lastName) {
        this.lastName = lastName;
    }

    @When("registering the customer")
    public void registeringTheCustomer() {
        var request = r.path("registerCustomer")
                .queryParam("cpr", this.cpr)
                .queryParam("bank_id", this.bank_id)
                .queryParam("firstName", this.firstName)
                .queryParam("lastName", this.lastName)
                .request();
        response = request.method("put");
    }

    @Then("the customer is registered")
    public void theCustomerIsRegistered() {
        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Then("the customer is not registered")
    public void theCustomerIsNotRegistered() {
        Assertions.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @When("registering the merchant")
    public void registeringTheMerchant() {
        var request = r.path("registerMerchant")
                .queryParam("cvr", this.cvr)
                .queryParam("bank_id", this.bank_id)
                .queryParam("firstName", this.firstName)
                .queryParam("lastName", this.lastName)
                .request();
        response = request.method("put");
    }

    @Then("the merchant is registered")
    public void theMerchantIsRegistered() {
        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Then("the merchant is not registered")
    public void theMerchantIsNotRegistered() {
        Assertions.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @When("updating the customer")
    public void updatingTheCustomer() {
        var request = r.path("updateCustomer")
                .queryParam("id", this.id)
                .queryParam("cpr", this.cpr)
                .queryParam("bank_id", this.bank_id)
                .queryParam("firstName", this.firstName)
                .queryParam("lastName", this.lastName)
                .request();
        response = request.method("put");
    }


    @When("updating the merchant")
    public void updatingTheMerchant() {
        var request = r.path("updateMerchant")
                .queryParam("id", this.id)
                .queryParam("cvr", this.cvr)
                .queryParam("bank_id", this.bank_id)
                .queryParam("firstName", this.firstName)
                .queryParam("lastName", this.lastName)
                .request();
        response = request.method("put");
    }

    @Then("the customer is updated")
    public void theCustomerIsUpdated() {
        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }

    @Then("the merchant is updated")
    public void theMerchantIsUpdated() {
        Assertions.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
    }


    @Then("the customer is not updated")
    public void theCustomerIsNotUpdated() {
        Assertions.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }

    @Then("the merchant is not updated")
    public void theMerchantIsNotUpdated() {
        Assertions.assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }
}
